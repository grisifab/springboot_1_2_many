package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot12ManyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot12ManyApplication.class, args);
	}

}
